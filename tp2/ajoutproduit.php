<?php
define('TARGET_DIRECTORY', './photo/');
if(!empty($_FILES['photo'])) {
    move_uploaded_file($_FILES['photo']['tmp_name'], TARGET_DIRECTORY . $_FILES['photo']['name']);
}
?>
<!DOCTYPE html>
<html>
<header>
    <meta charset="UTF-8">
    <title>Ajout d'un Produit</title>
</header>

<body>

<form action="./" method="POST" enctype="multipart/form-data">
    <fieldset title="Ajout d'un Produit">
        <legend>Ajout d'un Produit</legend>
        <p>
            <label for="vdname">Nom du Vendeur</label>
            <input type="text" name="vdname" id="vdname" />
        </p>
        <p>
            <label for="productname">Nom du Produit</label>
            <input type="text" name="productname" id="productname" required/>
        </p>
        <p>
            <label for="price">Prix du Produit</label>
            <input type="number" min="0" step="0.01" name="price" id="price" required/>
        </p>
        <p>
            <label for="quantity">Quantité</label>
            <input type="number" min="0" step="1" name="quantity" id="quantity" required />
        </p>
        <p>
            <label for="photo">Photo du Produit</label>
            <input type="file" name="photo" id="photo" accept=".jpg, .jpeg, .png" required />
        </p>
        <p>
            <input type="submit" value="Mettre en Vente"/>
        </p>
    </fieldset>
</form>
    <a href="listeprod.php">Liste des Produits</a>
</body>
</html>