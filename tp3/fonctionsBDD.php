<?php
function initialisation(){
    $bdd = new PDO('mysql:host=localhost;dbname=chat;charset=utf8','root','isencir');
    return $bdd;
}

function preparation($bdd){
    $statement = $bdd->prepare('INSERT INTO message(utilisateur,text,post_date) VALUES(:utilisateur, :text, :post_date)');
    $statement->execute([":utilisateur"=>$_POST['utilisateur'],":text"=>$_POST['message'],':post_date'=>date('y-M-d H:i:s')]);
}

function affichage($bdd){
    $rep= $bdd->query('SELECT * FROM message');
    while($donnee = $rep->fetch()){
        echo $donnee['post_date'].' '.$donnee['utilisateur'].':'.$donnee['text'].'</br>';
    }
    
}

?>

