<!DOCTYPE html>
<?php session_start();
      include'calendarFunctions.php'?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Kalendhar</title>
        <style>
            #menu{border: 1px solid black;margin: auto; height:40px;}
            #day{border: 1px solid black; margin: auto; height:25px;}
            td{border: 1px solid black;margin: auto; height: 80px; width:200px;}
            table{display:flex ;justify-content: center;}
        </style>
    </head>
    
    <body>
        <table>
            <tr>
                <td id='menu'>Compte: <?php echo $_SESSION['username']; ?></td>
                <td id='menu'>
                    <form method="POST" action="monthchange.php">
                        <input type="submit" name="previousc" value="previous" >
                    </form>
                </td> 
                <td id='menu'><?php echo date("Y - F",mktime(0,0,0,$_SESSION['month']+1,0,$_SESSION['year']));?></td>
                <td id='menu'>
                    <form method="POST" action="monthchange.php">
                        <input type="submit" name="nextc" value="next" >
                    </form>
                </td>
                <td id='menu'>
                <form method="POST" action="index.php">
                        <input type="submit" name="logout" value="Log Out" >
                </form>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td id='day'>Monday</td>
                <td id='day'>Tuesday</td>
                <td id='day'>Wednesday</td>
                <td id='day'>Thursday</td>
                <td id='day'>Friday</td>
                <td id='day'>Saturday</td>
                <td id='day'>Sunday</td>
            </tr>
            <?php
            displayCalendarCu($_SESSION['month'],$_SESSION['year']);
            ?>
        </table>
    </body>
</html>

