<?php
/* Fonction d'affiche d'un tableau sous la forme d'une grille, donc visible sur la page HTML
 * C'est pour cela qu'on utilise notre tableau en paramètre */
function Grille($tablau){
    echo "<table>";
    for ($i = 0; $i<20; $i++){
        echo "<tr>";
           for($j = 0; $j<55; $j++){
               if($tablau[$i][$j]==1){
                   echo "<td>O</td>"; 
               }
               else{
                   echo "<td> </td>"; 
               }
          }
        echo "</tr>";
    }
    echo"</table>";
}

function FirstTab(){
    $tablo=array();
    for($i=0;$i<20;$i++){
        array_push($tablo,[]);
    }
    
    for($i=0;$i<20;$i++){
        for($j=0;$j<55;$j++){
            array_push($tablo[$i], rand(0,1));
        }
    }
    
    return $tablo;
}

function showTab($tableau){
    echo '<pre>';
    print_r($tableau);
    echo '</pre>';
}

function Survie($tableo,$i,$j){
    $voisins = 0; $vie = 0;
    for($k = $i-1; k<= $i+1; $k++){
        for($t = $j-1; $t<=$j+1; $t++){
            if($k==$i && $t==$j){
                $voisins=$voisins;
            }
            else{
                if($tableo[k][j]==1){
                    $voisins=$voisins+1;
                }
            }
        }
    }
    if($voisins==2 || $voisins==3){
        $vie = 1;
    }
    
    return $vie;
}

function Naiss($tableo,$i,$j){
    $voisins = 0; $vie = 0;
    for($k = $i-1; k<= $i+1; $k++){
        for($t = $j-1; $t<=$j+1; $t++){
            if($k==$i && $t==$j){
                $voisins=$voisins;
            }
            else{
                if($tableo[k][j]==1){
                    $voisins=$voisins+1;
                }
            }
        }
    }
    if($voisins == 3){
        $vie=1;
    }
    
    return $vie;
}




?>

